/**
 * Created by gustavog on 14/07/17.
 */
(function($){

    $.extend({
        "effect":function(url,eff,reverse){
            if(!reverse) reverse = false;
            effect(url,eff,reverse);
        },
        "effect_init":function(){
            $('[data-transition="slide"]').each(function(i,el){
                $(el).click(function(ev){
                    ev.preventDefault();
                    effect($(this).attr("href"),$(this).data("transition"),$(this).data("direction") == "reverse");
                    //location.href = $(thi).attr("href");
                });
            });
        }
    });

    function effect(url,eff,r){
        $.ajax({
            url:url,
            success:function(e){
                e = e.replace(new RegExp("[<]script[ ]+.+cordova\.js.+[<][/]script[>]","g"),"");
                var div = $("<div>");
                var temp = $(e);
                temp.find(".headerStatic").remove();
                div.append(e);
                let top = $(".body").offset().top;
                div.css({
                    "position":"absolute",
                    "top":top,
                    "left":$(".body").width()+"px",
                    "width":$(".body").width()+"px"
                });
                $(".body").append(div);
                $(".body").addClass("animation");
                $(div).addClass("animation");
                $(".body").addClass(eff+"Out"+(r?"R":""));
                $(div).addClass(eff+"In"+(r?"R":""));

//jqueryui porque?
                setTimeout(function () {
                    location.href = url;
                },800);
            }
        });
    }
    $(document).ready(function(){
        $.effect_init();
    });
})(jQuery);