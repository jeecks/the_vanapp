$(document).ready(function () {
    $("#form2").hide();
    $(".photobox").css("background-image", "url(img/loading.gif)");
    $('select').material_select();
    //debo reiniciar el select porque materialize vale verga
    $("select").on('change', function () {
        $('select').material_select();
    });
    
    setBack(function () {
        $(".back").click();
    });
    
    $(".validator").on("click", function (e) {
        switch ($(this).attr("data-usage")) {
            case "search":
            var a = $(this);
            e.preventDefault();
            e.stopPropagation();
            if (!$("#form1").valid({
                showErrors: function (errorMap, errorList) {
                    return true;
                }
            })) {
                return toastError("Hay errores en el formulario", false);
            }
            $.ajax({
                type: "POST",
                crossDomain: true,
                beforeSend: function () {
                    appendLoader();
                },
                url: getUrl("config/astudent1"),
                data: $("#form1").serialize(),
                success: function (response) {
                    response = JSON.parse(response);
                    removeLoader();
                    console.log(response);
                    if (!response.success) {
                        toastError(response.message, false);
                    } else {
                        var vivienda = response.response.vivienda;
                        var lat = vivienda.substr(0, vivienda.indexOf(','));
                        var lng = vivienda.substr(vivienda.indexOf(',') + 1);
                        toastSuccess("Usuario encontrado", false);
                        $("#form1").hide();
                        $("#nombre").val(response.response.nombre);
                        $("#apellido").val(response.response.nombre);
                        $("#apellido").val(response.response.apellido);
                        $("#cedulaf").val(response.response.cedula);
                        $("#stud").val(response.response.id);
                        $("#id").val(function () {
                            var sid = getSession(true);
                            return sid.cservicio_id;
                        })
                        $(".photobox").css("background-image", "url(" + getSrc(response.response.imagen) + ")");
                        Materialize.updateTextFields();
                        $("#form2").show();
                        loadStaticMap(parseFloat(lat), parseFloat(lng), $(".map-salvaje"));
                        showMapNoSelect($("#map"), $(".map-salvaje"), parseFloat(lat), parseFloat(lng));
                    }
                }
            });
            break;
            
            case "asoc":
            var a = $(this);
            e.preventDefault();
            e.stopPropagation();
            var datos = {
                "id": $("#stud").val(),
                "servicio_id": $("#id").val()
            };
            $.ajax({
                type: "POST",
                crossDomain: true,
                beforeSend: function () {
                    appendLoader();
                },
                url: getUrl("config/astudent2"),
                data: datos,
                success: function (response) {
                    response = JSON.parse(response);
                    removeLoader();
                    console.log(response);
                    if (!response.success) {
                        toastError(response.message, false);
                    } else {
                        sendNotif($("#stud").val(),"Te han agregado a un servicio!",{"action":"service"},function(){                            toastSuccess(response.response,false);
                            setTimeout(window.location.href = "cservices.html",2000);});
                        }
                    }
                });
                break;
                
                default:
                toastError("Error, debe reiniciar la aplicación", false);
                break;
            }
        });
    });