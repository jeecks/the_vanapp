$(document).ready(function(){
    var unicode;
    var user = getSession(true);  
    var url = new URL(location.href);
    var from = url.searchParams.get("from");
    $("#back").attr("href", (from == null ? "services" : from) + ".html");
    
    $('select').material_select();    
    $("select").on('change', function () {
        unicode = $("select[name=univ]").val();
        $('select').material_select();
        $(".uniname").html($("input.select-dropdown").val());
        $(".unipic").attr("src","img/"+ unicode.toLowerCase() + ".jpg");        
        $.ajax({
            type: "POST",
            url: getUrl("institute/GetInstituteInfo"),
            data: "id=" + getUniId(unicode),  
            success: function (response) {
                var json = JSON.parse(response);
                if(json.success){                    
                    $(".dir").text(json.response.direccion);
                } else {
                    $(".dir").text("Dirección no disponible.");
                }
            }
        });
    });
    if(user.rol == "conductor"){
        $(".continue").on('click', function () {
            unicode = $("select[name=univ]").val();              
            $.ajax({
                type: "POST",
                url: getUrl("institute/ChangeInstituteService"),
                data: "idinst=" + getUniId(unicode) + "&servic=" + user.cservicio_id, 
                beforeSend: function() {
                    appendLoader();
                },  
                success: function (response) {
                    var json = JSON.parse(response);
                    console.log(json);
                    if(json.success){
                        var user = getSession(true);                    
                        setSession(user);  
                        user.servicio_id = null; 
                        user.instituto_id = getUniId(unicode);             
                        toastSuccess("¡Cambio de instituto realizado correctamente!", false); 
                        setTimeout(refreshSes());                   
                    } else {
                        toastError(json.message, false);
                    }
                    removeLoader();
                    loadInfo(user.id);
                }
            });
        });
    }else{
        $(".continue").on('click', function () {
            unicode = $("select[name=univ]").val();              
            $.ajax({
                type: "POST",
                url: getUrl("institute/ChangeInstitute"),
                data: "idinst=" + getUniId(unicode) + "&iduser=" + user.id, 
                beforeSend: function() {
                    appendLoader();
                },  
                success: function (response) {
                    var json = JSON.parse(response);
                    console.log(json);
                    if(json.success){    
                        var user = getSession(true); 
                        user.servicio_id = null;
                        user.instituto_id = getUniId(unicode);                    
                        setSession(user);                
                        toastSuccess("¡Cambio de instituto realizado correctamente!", false);                    
                    } else {
                        toastError(json.message, false);
                    }
                    removeLoader();
                    loadInfo(user.id);
                }
            });
        });
    }
    $("#change").on("click",function(e){
        e.preventDefault();        
    });
    
    loadInfo(user.id);
});

function validate(){
    var valid = true;
    
    if($("#institute").val() == undefined || $("#institute").val() == ""){
        valid = false;
        if(!$("#institute").hasClass("invalid"))
        $("#institute").toggleClass("invalid");  
    }
    
    return valid;
}

function loadInfo(iduser){
    $.ajax({
        type: "POST",
        url: getUrl("user/GetInstituteInfo"),
        data: "id=" + iduser,    
        async: false,
        beforeSend: function() {         
            appendLoader(); 
        },    
        success: function (response) {
            var json = JSON.parse(response);
            if(json.success){
                var unicode = getUnicode(json.response.id);      
                $("select").find('option[value="'+unicode+'"]').prop('selected', true);
                $("select").val(unicode);                                          
                $(".unipic").attr("src","img/"+ unicode.toLowerCase() + ".jpg");
                $('select').material_select();
                $(".uniname").text($("input.select-dropdown").val());
                $(".dir").text(json.response.direccion);
            } else {
                toastError(json.message, false);
            }
            removeLoader();
        }
    });
}

function getUniId(stud){
    switch(stud){
        case "URBE":
        return 1;
        break;
        case "UNICA":
        return 2;
        break;
        case "LUZING":
        return 3;
        break;
        case "LUZMED":
        return 4;
        break;
        case "LUZVET":
        return 5;
        break;
    }
}

function getUnicode(id){
    switch(id){
        case "1":
        return "URBE";
        break;
        case "2":
        return "UNICA";
        break;
        case "3":
        return "LUZING";
        break;
        case "4":
        return "LUZMED";
        break;
        case "5":
        return "LUZVET";
        break;
        default:
        return "null"
        break;
    }
}