$(document).ready(function(){
    var user = JSON.parse(getSession());
    var url = new URL(location.href);
    var from = url.searchParams.get("from");

    $("#back").attr("href", (from == null ? "services" : from) + ".html");
    $("#change").on("click", function (e) {
        var a = $(this);                
        if(validate()){
            console.log("id=" + user.id + "&oldpass=" + $("#actualpass").val() + "&newpass=" + $("#newpass").val());
            $.ajax({
                type: "POST",
                url: getUrl("config/ChangePassword"),
                data: "id=" + user.id + "&oldpass=" + $("#actualpass").val() + "&newpass=" + $("#newpass").val(),            
                success: function (response) {                    
                    var json = JSON.parse(response);
                    console.log(json);
                    if(!json.success)
                        toastError(json.message, false);
                    else{
                        toastSuccess(json.response, false);
                        $("#actualpass").val("");
                        $("#newpass").val("");
                        $("#repass").val("");
                    }                                    
                }
            });
        }        
    });    
});

function validate() {
    var valid = true;

    if($("#actualpass").val() == undefined || $("#actualpass").val() == ""){
        valid = false;
        if(!$("#actualpass").hasClass("invalid"))
            $("#actualpass").toggleClass("invalid");  
    } else {
        if($("#actualpass").hasClass("invalid"))
            $("#actualpass").toggleClass("invalid"); 
    }
    
        
    if($("#newpass").val() == undefined || $("#newpass").val() == ""){
        valid = false;
        if(!$("#newpass").hasClass("invalid"))
            $("#newpass").toggleClass("invalid");  
    } else {
        if($("#newpass").hasClass("invalid"))
            $("#newpass").toggleClass("invalid"); 
    }

    if($("#repass").val() == undefined || $("#repass").val() == ""){
        valid = false;
        if(!$("#repass").hasClass("invalid"))
            $("#repass").toggleClass("invalid");  
    } else {
        if($("#repass").hasClass("invalid"))
            $("#repass").toggleClass("invalid"); 
    }

    if(!valid){         
        toastWarning("Uno de los campos esta vacío", false);          
        return false;
    }

    if($("#newpass").val() != $("#repass").val()){
        valid = false;
        if(!$("#newpass").hasClass("invalid"))
            $("#newpass").toggleClass("invalid");  
        if(!$("#repass").hasClass("invalid"))
            $("#repass").toggleClass("invalid");  
    }

    if(!valid){
        toastWarning("Las contraseñas no coinciden", false);                   
        return false;
    } else {
        if($("#pass").hasClass("invalid"))
            $("#pass").toggleClass("invalid");  
        return true;
    }
    
}