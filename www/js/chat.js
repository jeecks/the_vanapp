var loading = false;
var lastMsg = false;
$(document).ready(function () {  
    var user = JSON.parse(getSession()); 
    $("#msg").click(function(){
        setTimeout(function(){
            $("#bodyPanel").animate({ scrollTop: $("#chatmessages").height() }, 1000); 
        },300);
    });
    loadChat();    
    $("#chatbox").css("height", $(document).height() - $("footer").height());
    $("#chatmessages").css("height", $(document).height() - $("footer").height());
    $("#chatmessages").css("margin-bottom", $("footer").height());
    $("#backb").on("click", function (e) {
        refreshSes();
    });
    $(document).scrollTop($(document).height());
    
});
var firstLoad = true;
function loadChat(){
    var user = JSON.parse(getSession());
    var id = user.servicio_id != null ? user.servicio_id : user.cservicio_id;
    
    if(id == null){
        refreshSes();
    }
    console.log("deberia hacer el request but...");
    $.ajax({
        type: "POST",
        async: false,
        url: getUrl("chat/GetMessages"),
        data: "id=" + id,
        success: function (response) {
            var json = JSON.parse(response);                  
            if(!json.success){
                toastInfo(json.message, false);        
            }else{
                $("#chatmessages").empty();
                console.log(json.response);
                $.each(json.response, function (index, msg) {    
                    var nombre = (msg.usuario_id == user.id ? "Tú" : msg.nombre+' '+msg.apellido) + (msg.servicio_id == msg.usuario_cservicio_id ? " (Conductor)" : "");                 
                    $("#chatmessages").append(
                        '<div class="msg z-depth-2">' +
                        '<div class="col s12 chatName">'+nombre+'</div>' +
                        '<div class="col s12">'+msg.chatgrupal+'<div class="msgTime">'+getTime(msg.time)+'</div></div>' +
                        '</div>'                    
                    );
                    lastMsg = msg.time;           
                });
            }
            if(firstLoad){
                $("#bodyPanel").animate({ scrollTop: $("#chatmessages").height() }, 1000); 
                firstLoad = false;
                setInterval(function(){getLastMsgs();},2000);  
            }
        }
    });    
}

function getLastMsgs(){
    //console.log(loading);
    //console.log(lastMsg);
    if(loading){
        return;
    }
    var user = JSON.parse(getSession());    
    var id = user.servicio_id != null ? user.servicio_id : user.cservicio_id;
    
    $.ajax({
        type: "POST",
        async: false,
        beforeSend: function(){loading = true},
        url: getUrl("chat/lastMessages"),
        data: {"id" : id, "lastmsg" : lastMsg},
        success: function (response) {
            var json = JSON.parse(response);                  
            if(!json.success){
                //toastInfo(json.message, false);        
            }else{
                console.log(json.response);
                $.each(json.response, function (index, msg) {
                    var nombre = (msg.id == user.id ? "Tú" : msg.nombre+' '+msg.apellido) + (msg.cservicio_id == user.cservicio_id ? " (Conductor)" : "");                 
                    $("#chatmessages").append(
                        '<div class="msg z-depth-2">' +
                        '<div class="col s12 chatName">'+nombre+'</div>' +
                        '<div class="col s12">'+msg.chatgrupal+'<div class="msgTime">'+getTime(msg.time)+'</div></div>' +
                        '</div>'                    
                    );
                    lastMsg = msg.time;
                    loading = false;
                });
                loading = false;
                removeLoader();
                $("#bodyPanel").animate({ scrollTop: $("#chatmessages").height() }, 1000); 
            }      
        }
    });    
}

function sendMsg(){
    var user = JSON.parse(getSession());
    var msg = $("#msg").val();
    $("#msg").val("");
    var id = user.servicio_id != null ? user.servicio_id : user.cservicio_id; 
    $.ajax({
        type: "POST",
        async: false,
        beforeSend: appendLoader(),
        url: getUrl("chat/send"),
        data: "id=" + user.id + "&msg=" + msg + "&idservicio=" + id,        
        success: function (response) {
            var json = JSON.parse(response); 
            console.log(json);
            sendChatNotif(id,user.nombre + ": " +msg,{"action" : "chat"});
            if(json.success){
                //lastMsg = json.response.time;
            }
            loading = false;
        }
    });
}


function getTime(time){
    time = new Date(time);    
    var hours   = time.getHours();
    var minutes = time.getMinutes(); 
    var timeString = "" + ((hours > 12) ? hours - 12 : hours);
    timeString  += ((minutes < 10) ? ":0" : ":") + minutes;
    timeString  += (hours >= 12) ? " P.M." : " A.M.";
    return timeString;
}