$(document).ready(function () {
    var ses = getSession(true);
    var loc = false;
    var inter = false;
    var url = new URL(location.href);
    var from = url.searchParams.get("from");

    $("#back").attr("href", (from == null ? "services" : from) + ".html");
    
    if(ses.rol == "estud"){
        $(".back").attr("href","services.html");
    }else{
        $(".back").attr("href","cservices.html");
    }
    
    setBack(function(){
        $(".back").click();
    });
    
    $("#ubic").prop('checked',getLocal("realUbic",true));
    $("#wifi").prop('checked',getLocal("wifiOnly",true));
    
    $("#ubic").on("change",function(){
        if($('#ubic').is(':checked')){
            cordova.plugins.diagnostic.isLocationEnabled(function(loc){
                if (loc){
                    toastSuccess("Ubicación en tiempo real activa!",false);
                    setLocal("realUbic",$('#ubic').is(':checked'));
                }else{
                    appendLoader();
                    var i = 0;
                    inter = setInterval(function(){
                        cordova.plugins.diagnostic.isLocationEnabled(function(loc){
                            if(loc){
                                toastSuccess("Ubicación en tiempo real activa!",false);
                                setLocal("realUbic",$('#ubic').is(':checked'));
                                clearInterval(inter);
                                removeLoader();
                            }else{
                                i++;
                                if(i != 3){
                                    toastError("No se activó la ubicación (" + i + "/3)",false);
                                }else{
                                    clearInterval(inter);
                                    setLocal("realUbic",false);
                                    $("#ubic").prop('checked',false);
                                    removeLoader();
                                }
                            }
                        },function(){
                        });
                    },2000);
                    if(confirm("Debe utilizar el GPS para habilitar esta opción. Desea habilitarlo?")){
                        cordova.plugins.diagnostic.switchToLocationSettings();
                    }else{
                        goTo("config.html",true);
                    }
                }
            }, function(){});
        }else{
            setLocal("realUbic",$('#ubic').is(':checked'));
            clearInterval(realPos);
        }
        
    });
    
    $("#wifi").on("change",function(){
        setLocal("wifiOnly",$('#wifi').is(':checked'));
    });
    
});