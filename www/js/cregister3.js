$(document).ready(function () {
    var uid = false;
    var unicode;
    
    setBack(function(){
        $(".back").click();
    });
    
    $('select').material_select();
    //debo reiniciar el select porque materialize vale verga
    
    $("select").on('change', function () {
        unicode = $("select[name=univ]").val();
        $('select').material_select();
        $(".unicode").html(unicode);
        $(".uniname").html($("input.select-dropdown").val());
        $(".unipic").css("background-image","url(../img/"+ unicode.toLowerCase() + ".jpg)");
    });
    
    function getUniId(stud){
        switch(stud){
            case "URBE":
            return 1;
            break;
            case "UNICA":
            return 2;
            break;
            case "LUZING":
            return 3;
            break;
            case "LUZMED":
            return 4;
            break;
            case "LUZVET":
            return 5;
            break;
        }
    }
    
    
    $(".validator").on("click", function (e) {
        var a = $(this);
        e.preventDefault();
        e.stopPropagation();
        if (!$("form").valid({
            showErrors: function (errorMap, errorList) {
                return true;
            }
        })) {
            return toastError("Hay errores en el formulario",false);
        } else if($("select[name=univ]").val() == null){
            return toastError("Debe seleccionar un instituto para continuar",false);
        }
        //hago toda la session completa
        var ses = getSession(true);
        ses.inst = getUniId(unicode);
        console.log(ses);
        setSession(ses);
        $.ajax({
            async: true,
            crossDomain: true,
            method: "POST",
            headers: {
                "content-type": "application/json",
                "cache-control": "no-cache"
            },
            beforeSend: function () {
                appendLoader();
            },
            url: getUrl("sesion/cvalidater3"),
            data: getSession(false),
            success: function (response) {
                response = JSON.parse(response);
                removeLoader();
                console.log(response);
                if (!response.success) {
                    toastError("Hubo un error en el registro",false);
                } else {
                    $.ajax({
                        async: true,
                        crossDomain: true,
                        method: "POST",
                        beforeSend: function () {
                            appendLoader();
                        },
                        url: getUrl("mailer/activationMail"),
                        data: {"email" :response.response.email},
                        success: function (response) {
                            response = JSON.parse(response);
                            removeLoader();
                            if (!response.success) {
                                toastError("Hubo un error en el registro, intentelo de nuevo",false);
                            } else {
                                goTo("emailverif.html",true);
                            }
                        }
                    });
                }
            }
        });
    });
    
});