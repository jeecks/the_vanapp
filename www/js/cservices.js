var base64g = false;
var cropit = false;
var geoAct = false;
var geoLoca = false;

appendLoader();
$(document).ready(function () {
  
  $(".notification-block").on("click",function(){
    var nombre = getSession(true);
    nombre = nombre.nombre;
    var stud =  $("#estudinfo").serializeJSON();
    stud = stud.estud; 
    
    console.log(nombre + " y " + stud)
    sendNotif(stud,nombre + " : " + $(this).html());
    $('#infoTab').click()
  });
  
  setBack(function () {
    if (confirm("Desea salir de la aplicación?")) {
      navigator.app.exitApp();
    }
  });
  
  $('select').material_select();
  //debo reiniciar el select porque materialize vale verga
  $("select").on('change', function () {
    $('select').material_select();
    if(geoLoca){
      clearInterval(geoLoca);
    }
    
    $.ajax({
      async: true,
      crossDomain: true,
      method: "POST",
      headers: {
        "content-type": "application/json",
        "cache-control": "no-cache"
      },
      beforeSend: function () {
        appendLoader();
      },
      url: getUrl("info/getStudentInfo"),
      data: JSON.stringify($("#estudinfo").serializeJSON()),
      success: function (response) {
        response = JSON.parse(response);
        $(".studentActions").show();
        if (response.success) {
          var stud = response.response;
          $(".photobox").css("background-image", "url(" + getSrc(stud.imagen) + ")");
          $("#callBTN").attr("href", stud.telefono);
          $("#sendSMSBTN").attr("href", stud.telefono);
          if (stud.actualpos != null) {
            $(".realActive").show();
            $(".realInactive").hide();
            geoLoc(response);
          } else {
            $(".realActive").hide();
            $(".realInactive").show();
          }
        } else {
          toastError("Error de servicio...");
        }
        removeLoader();
      }
    });
  });
  
  function geoLoc(responsel) {
    $.ajax({
      async: true,
      crossDomain: true,
      method: "POST",
      headers: {
        "content-type": "application/json",
        "cache-control": "no-cache"
      },
      beforeSend: function () {
        appendLoader();
      },
      url: getUrl("info/getPos"),
      data: JSON.stringify({"estud" : responsel.response.id}),
      success: function (response) {
        response = JSON.parse(response);
        console.log(response);
        $(".studentActions").show();
        if (response.success) {
          if (!geoAct) {
            var pos = response.response.actualpos;
            var lat = pos.substr(0, pos.indexOf(','));
            var lng = pos.substr(pos.indexOf(',') + 1);
            loadStaticMap(parseFloat(lat), parseFloat(lng), $(".map-salvaje"));
            showMapNoSelect($("#map"), $(".map-salvaje"), parseFloat(lat), parseFloat(lng));
            geoAct = true;
          } else {
            var pos = response.response.actualpos;
            var lat = pos.substr(0, pos.indexOf(','));
            var lng = pos.substr(pos.indexOf(',') + 1);
            var myLatlng = new google.maps.LatLng(lat, lng);
            placeMarkerStat(myLatlng, $(".map-salvaje"));
          }
        } else {
          toastError("Error de servicio...");
        }
        removeLoader();
      }
    });
    geoLoca = setInterval(function () {
      $.ajax({
        async: true,
        crossDomain: true,
        method: "POST",
        headers: {
          "content-type": "application/json",
          "cache-control": "no-cache"
        },
        beforeSend: function () {
          appendLoader();
        },
        url: getUrl("info/getPos"),
        data: JSON.stringify({"estud" : responsel.response.id}),
        success: function (response) {
          response = JSON.parse(response);
          console.log(response);
          $(".studentActions").show();
          if (response.success) {
            if (!geoAct) {
              var pos = response.response.actualpos;
              var lat = pos.substr(0, pos.indexOf(','));
              var lng = pos.substr(pos.indexOf(',') + 1);
              loadStaticMap(parseFloat(lat), parseFloat(lng), $(".map-salvaje"));
              showMapNoSelect($("#map"), $(".map-salvaje"), parseFloat(lat), parseFloat(lng));
              geoAct = true;
            } else {
              var pos = response.response.actualpos;
              var lat = pos.substr(0, pos.indexOf(','));
              var lng = pos.substr(pos.indexOf(',') + 1);
              var myLatlng = new google.maps.LatLng(lat, lng);
              placeMarkerStat(myLatlng, $(".map-salvaje"));
            }
          } else {
            toastError("Error de servicio...");
          }
          removeLoader();
        }
      });
    }, 10000);
    
  }
  
  
  var infomap = false;
  var actualtab = "services";
  var ses = getSession(true);
  $("#id").val(ses.id);
  $("#cidinst").val(ses.instituto_id);
  $.ajax({
    async: true,
    crossDomain: true,
    method: "POST",
    headers: {
      "content-type": "application/json",
      "cache-control": "no-cache"
    },
    url: getUrl("services/getConductorService"),
    data: JSON.stringify({
      "id": ses.id
    }),
    success: function (response) {
      response = JSON.parse(response);
      if (!response.success) {
        removeLoader();
        toastInfo("Bienvenido, llene el campo para publicar su servicio", false);
        return;
      };
      console.log(response);
      var serv = response.response.servicio;
      var vehi = response.response.vehiculos;
      var marca = response.response.marca;
      if (serv.matutino == 1) {
        $("input[name='matutino']").prop('checked', true);
      }
      if (serv.vespertino == 1) {
        $("input[name='vespertino']").prop('checked', true);
      }
      if (serv.nocturno == 1) {
        $("input[name='nocturno']").prop('checked', true);
      }
      $("#marca").val(marca.nombre).prop("disabled", true);
      $("#modelo").val(vehi.model).prop("disabled", true);;
      $("#placa").val(vehi.placa).prop("disabled", true);;
      $("#year").val(vehi.año).prop("disabled", true);;
      $("#asientos").val(serv.asientos_max);
      $("img.selpic").attr("src", getSrc(serv.img));
      $("#descripcion").val(serv.descripcion);
      var sesion = getSession(true);
      sesion["cservicio_id"] = serv.id;
      setSession(sesion);
      removeLoader();
    }
  });
  
  $(".selpic").on("click", function () {
    $("#pic").click();
  });
  
  $("#pic").change(function () {
    if (this.files && this.files[0]) {
      $(".modal2").click();
      var reader = new FileReader();
      reader.onload = imageIsLoaded;
      reader.readAsDataURL(this.files[0]);
    }
  });
  
  function imageIsLoaded(e) {
    var img = $('.cropit').attr("src", e.target.result);
    var el = img.get(0);
    cropit = new Croppie(el, {
      enableExif: true,
      viewport: {
        width: 250,
        height: 150,
        type: 'square'
      },
      boundary: {
        width: 300,
        height: 300
      }
    });
  };
  
  $(".crop-image").on("click", function () {
    console.log(
      $(cropit.result('base64').then(function (base64) {
        $("img.selpic").attr("src", base64);
        base64g = base64;
      })));
      cropit.destroy();
    });
    $(".destroy").on("click", function () {
      cropit.destroy();
    });
    
    function returnPagado(le) {
      if (le.pagado == 0) {
        return '<input type="checkbox" class="filled-in" id="' + le.id + '" name="' + le.id + '" /> <label for="' + le.id + '">Pagado?</label> ';
      } else {
        return '<input type="checkbox" class="filled-in" id="' + le.id + '" name="' + le.id + '" checked=checked  disabled/> <label for="' + le.id + '">Pagado?</label>';
      }
    }
    
    function assignDetail() {
      $(".feedetail").on("click", function () {
        var target = $(this).attr("data-target");
        console.log("nani?");
        $.ajax({
          async: true,
          crossDomain: true,
          method: "POST",
          headers: {
            "content-type": "application/json",
            "cache-control": "no-cache"
          },
          url: getUrl("payments/GetPaymentDetail"),
          data: JSON.stringify({
            "cuotaid": target
          }),
          beforeSend: function () {
            appendLoader();
            $(".studbody").empty();
          },
          success: function (response) {
            response = JSON.parse(response);
            console.log(response);
            if (response.success) {
              var detail = response.response;
              console.log(detail.length);
              $(detail).each(function (index, detail) {
                var pag = returnPagado(detail);
                $(".studbody").append($('<tr>' +
                '<td>' + detail.nombre + ' ' + detail.apellido + '</td>' +
                '<td>' + pag + '</td>' +
                '</tr>'
              ));
            });
            $(".updatepago").attr("data-cuota", target);
            removeLoader();
            return;
          };
        }
      });
    });
  }
  $(".updatepago").on("click", function () {
    var target = $(this).attr("data-cuota");
    var claro = [];
    $(".studbody .filled-in").each(function (index, element) {
      claro.push({
        "id": $(element).attr("id"),
        "status": $(element).prop("checked")
      });
    });
    $.ajax({
      async: true,
      crossDomain: true,
      method: "POST",
      headers: {
        "content-type": "application/json",
        "cache-control": "no-cache"
      },
      url: getUrl("payments/updatePayments"),
      data: JSON.stringify({
        "cuotas": claro,
        "cuotaid": target
      }),
      beforeSend: function () {
        appendLoader();
      },
      success: function (response) {
        response = JSON.parse(response);
        if (response.success) {
          $('#modal3').modal('close');
          toastSuccess(response.response, false);
        } else {
          toastError("Error al actualizar cuotas", false);
        }
        removeLoader();
      }
    });
  });
  $(".tabs a").on("click", function (e) {
    switch ($(this).attr("data-target")) {
      case "services":
      if(geoLoc){
        clearInterval(geoLoca);
      }
      actualtab = "services";
      break;
      case "payments":
      if(geoLoc){
        clearInterval(geoLoca);
      }
      if (getLocal("payments") == null) {
        setLocal("payments", true);
        setTimeout(function () {
          $('.tap-target').tapTarget('open');
        }, 1000)
      }
      console.log("que verga?");
      var ses = getSession(true);
      if (actualtab != "payments") {
        console.log("que verga?");
        $.ajax({
          async: true,
          crossDomain: true,
          method: "POST",
          headers: {
            "content-type": "application/json",
            "cache-control": "no-cache"
          },
          url: getUrl("services/GetPayments"),
          data: JSON.stringify({
            "id": ses.cservicio_id
          }),
          beforeSend: function () {
            appendLoader();
            $("ul.fees").empty();
          },
          success: function (response) {
            response = JSON.parse(response);
            console.log(response);
            if (response.success) {
              var cuotas = response.response;
              
              console.log(response);
              for (let index = 0; index < cuotas.length; index++) {
                var cuota = cuotas[index];
                $("ul.fees").append($('<li class="fee">' +
                '<div class="collapsible-header">' +
                ' <div class="col s12 feename">' + cuota.nombre + '</div>' +
                '</div>' +
                '<div style="padding:1rem;" class="collapsible-body">' +
                '<div class="row">' +
                '<div class="col s12">' +
                '<div class="row">' +
                '<div class="col s12 left-align nopadding subtitle">' +
                '<strong>Fecha de Pago</strong>' +
                '</div>' +
                '<div class="col s12 left-align nopadding info ">' + cuota.fecha + '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col s12 left-align nopadding subtitle">' +
                '<strong>Monto</strong>' +
                '</div>' +
                '<div class="col s12 left-align nopadding info ">BsF ' + parseFloat(cuota.cantidad).toLocaleString('es') + '</div>' +
                '<br><div style="margin: 1em 0em;" class="col s12 left-align nopadding info center "><a class="feedetail btn modal-trigger" href="#modal3" data-target="' + cuota.id + '">VER DETALLES</a></div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</li>'
              ));
            }
          }
          assignDetail();
          removeLoader();
        }
        
      });
    }
    actualtab = "payments";
    break;
    case "info":
    //aqui va el ajax que se trae los datos de posición del user xd
    if (actualtab != "info") {
      var ses = getSession(true);
      $(".studentActions").hide();
      console.log("el coño de la madre");
      $.ajax({
        async: true,
        crossDomain: true,
        method: "POST",
        headers: {
          "content-type": "application/json",
          "cache-control": "no-cache"
        },
        beforeSend: function () {
          appendLoader();
        },
        url: getUrl("info/getStudents"),
        data: JSON.stringify({
          "id": ses.cservicio_id
        }),
        success: function (response) {
          response = JSON.parse(response);
          console.log(response);
          $("#estud").empty();
          $(".photobox").css("background-image","unset");
          if (response.success) {
            $("#estud").append("<option disabled selected>Seleccione un estudiante</option>");
            $(response.response).each(function (i, e) {
              $("#estud").append("<option value='" + e.id + "'>" + e.nombre + " " + e.apellido + "</option>");
            });
            $('select').material_select();
            toastInfo("Seleccione un estudiante para continuar", false);
          } else {
            toastError(response.message, false);
          }
          removeLoader();
        }
      });
    }
    actualtab = "info";
    break;
    default:
    break;
  }
});

$('.datepicker').pickadate({
  selectMonths: true, // Creates a dropdown to control month
  selectYears: 15, // Creates a dropdown of 15 years to control year,
  today: 'Today',
  clear: 'Clear',
  close: 'Ok',
  closeOnSelect: false, // Close upon selecting a date,
  onClose: function () {
    $(document.activeElement).blur();
  }
});

$(".addCuota").on("click", function (e) {
  var a = $(this);
  e.preventDefault();
  e.stopPropagation();
  var ses = getSession(true);
  $("#serid").val(ses.cservicio_id);
  if (!$("#formcuota").valid({
    showErrors: function (errorMap, errorList) {
      return true;
    }
  })) {
    return toastError("Hay errores en el formulario", false);
  }
  $.ajax({
    async: true,
    crossDomain: true,
    method: "POST",
    headers: {
      "content-type": "application/json",
      "cache-control": "no-cache"
    },
    beforeSend: function () {
      appendLoader();
    },
    url: getUrl("payments/assignPayment"),
    data: JSON.stringify($("#formcuota").serializeJSON()),
    success: function (response) {
      response = JSON.parse(response);
      removeLoader();
      if (response.success) {
        console.log(response);
        $.ajax({
          async: true,
          crossDomain: true,
          method: "POST",
          beforeSend: function () {
            $("ul.fees").empty();
          },
          headers: {
            "content-type": "application/json",
            "cache-control": "no-cache"
          },
          url: getUrl("services/GetPayments"),
          data: JSON.stringify({
            "id": ses.cservicio_id
          }),
          success: function (response) {
            response = JSON.parse(response);
            if (response.success) {
              var cuotas = response.response;
              
              console.log(response);
              for (let index = 0; index < cuotas.length; index++) {
                var cuota = cuotas[index];
                $("ul.fees").append($('<li class="fee">' +
                '<div class="collapsible-header">' +
                ' <div class="col s12 feename">' + cuota.nombre + '</div>' +
                '</div>' +
                '<div style="padding:1rem;" class="collapsible-body">' +
                '<div class="row">' +
                '<div class="col s12">' +
                '<div class="row">' +
                '<div class="col s12 left-align nopadding subtitle">' +
                '<strong>Fecha de Pago</strong>' +
                '</div>' +
                '<div class="col s12 left-align nopadding info ">' + cuota.fecha + '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col s12 left-align nopadding subtitle">' +
                '<strong>Monto</strong>' +
                '</div>' +
                '<div class="col s12 left-align nopadding info ">BsF ' + parseFloat(cuota.cantidad).toLocaleString('es') + '</div>' +
                '<br><div style="margin: 1em 0em;" class="col s12 left-align nopadding info center "><a class="feedetail btn" data-target="' + cuota.id + '">VER DETALLES</a></div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</li>'
              ));
            }
            return;
          };
        }
      });
      $('#modal1').modal('close');
    } else {
      toastError(response.message, false);
    }
  }
});
});

$(".validator").on("click", function (e) {
  var a = $(this);
  e.preventDefault();
  e.stopPropagation();
  if (!$("form").valid({
    showErrors: function (errorMap, errorList) {
      return true;
    }
  })) {
    return toastError("Hay errores en el formulario", false);
  } else if (!base64g) {
    if (!$(".selpic").attr("src").includes("studio-brown")) {
      return toastError("Subir una imagen del transporte es obligatorio", false);
    }
  }
  var ses = getSession(true);
  
  var form = $("form").serializeJSON();
  if (ses.cservicio_id != null){
    form.cservicio_id = ses.cservicio_id;
  }else{
    form.cservicio_id = null;
  }
  form.matutino = $("input[name='matutino']").is(":checked");
  form.vespertino = $("input[name='vespertino']").is(":checked");
  form.nocturno = $("input[name='nocturno']").is(":checked");
  form.pic = base64g;
  $.ajax({
    async: true,
    crossDomain: true,
    method: "POST",
    headers: {
      "content-type": "application/json",
      "cache-control": "no-cache"
    },
    beforeSend: function () {
      appendLoader();
    },
    url: getUrl("services/putService"),
    data: JSON.stringify(form),
    success: function (response) {
      response = JSON.parse(response);
      removeLoader();
      if (response.success) {
        console.log(response);
        var response = response.response;
        if (typeof response.activo != null) {
          var ses = getSession(true);
          setTimeout(1500,refreshSes());
          toastSuccess(response, false);
        } else {
          toastSuccess(response, false);
        }
      } else {
        toastError(response.message, false);
      }
    }
  });
});

});