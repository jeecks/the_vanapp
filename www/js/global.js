//SENDER ID 590432625734
var url = "https://studio-brown.com";
//var url = "http://localhost";
//var url = "http://192.168.0.103";
var bfn = false;
onLoad();
var realPos = false;
var stickyNotif = false;

function onLoad(){
    document.addEventListener("deviceready", onDeviceReady, false);
}

function sendNotif(id,sms,params,callback){
    var parametros;
    if(typeof params == "undefined"){
        parametros = {};
    }else{
        parametros = params;
    }
    var data = {
        "title" : "Vanapp",
        "message" : sms,
        "image" : "www/img/icon.png",
        "sound" : "open"
    }
    
    data = $.extend({},parametros,data);
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: getUrl("notif/sendNotification"),
        data: {"id" : id, "data" : data},
        success: function (response) {
            console.log(response);
            response = JSON.parse(response);
            if(response.success){
                toastSuccess("Notificación enviada",false);
            }else{
                toastError("El usuario no se encuentra disponible",false);
            }
            if(typeof callback != undefined){
                callback();
            }
        }
    });
}

function refreshSes(){
    var ses = getSession(true);
    $.ajax({
        type: "POST",
        crossDomain: true,
        beforeSend: function () {
            appendLoader();
        },
        url: getUrl("sesion/login"),
        data: "email=" + ses.email + "&password=" + ses.pass,
        success: function (response) {
            console.log(response);
            response = JSON.parse(response);
            console.log($(response).get(0));
            if (!response.success) {
                //toastError(response.message,false);
                removeLoader();
            } else {
                ses = response.response;
                setSession(ses);
                updateKey(function(){
                    removeLoader();
                    if (ses.conductor == 1){
                        var rol = getSession(true);
                        rol.rol = "conductor";
                        setSession(rol);
                        goTo("cservices.html",true);
                        return;
                    }else{
                        var rol = getSession(true);
                        rol.rol = "estud";
                        setSession(rol);
                        goTo("services.html",true);
                        return;
                    }
                });
                
            }
        }
    });
}

function sendChatNotif(id,sms,params){
    var parametros;
    if(typeof params == "undefined"){
        parametros = {};
    }else{
        parametros = params;
    }
    
    var data = {
        "title" : "Vanapp",
        "message" : sms,
        "image" : "www/img/icon.png",
        "sound" : "open"
    }
    
    data = $.extend({},parametros,data);
    
    
    
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: getUrl("notif/sendChatNotification"),
        data: {"id" : id, "data" :data},
        success: function (response) {
            console.log(response);
            response = JSON.parse(response);
            if(response.success){
                // toastSuccess("Notificación enviada",false);
            }else{
                //  toastError("El usuario no se encuentra disponible",false);
            }
        }
    });
}

function getPage(){
    var path = window.location.pathname;
    var page = path.split("/").pop();
    return page;
}

function updateKey(callback){
    var id;
    if(getSession(true)){
        id = getSession(true);
        id = id.id;
    }else{
        return false;
    }
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: getUrl("notif/updateDevice"),
        data: {"id" : id, "devKey" : getLocal("devKey",false)},
        success: function (response) {
            if(!response.success){
                callback();
            }
        }
    });
}

function getPush(){
    var push = null;
    var ses = getSession(true);
    if(getPage() == "groupchat.html" || (getPage() == "services.html") && ses.servicio_id == null){
        push = PushNotification.init({ "android": {"senderID": "590432625734"},
        "ios": {"alert": "true",  "badge": "true", "sound": "true"}, "windows": {} } );
    }else{
        push = PushNotification.init({ "android": {"senderID": "590432625734","forceShow": "true" },
        "ios": {"alert": "true", "badge": "true" , "sound": "true"}, "windows": {} } );
    }
    push.on('registration', function(data) {
        if(data == null){
            return;
        }else{
            setLocal("devKey", data.registrationId);
        }
    });
    
    push.on('notification', function(data) {
        console.log(data);
        if(typeof data.additionalData == "undefined"){
            return;
        }else{
            var rol = getSession(true);
            rol = rol.rol;
            switch(data.additionalData.action){
                case "chat":
                if(!data.additionalData.foreground){
                    goTo("groupchat.html",true);
                }
                break;
                case "autonotif":
                var studid = data.additionalData.studid;
                if(rol == "estud"){
                    goTo("services.html?go=info",true);
                }
                else{
                    goTo("cservices.html?go=info&studid="+studid,true);
                }
                break;
                case "cuotas":
                goTo("services.html?go=payments",true);
                break;
                case "service":
                refreshSes();
                //goTo("services.html",true);
                default:
                break;
            }
        }
    });
    
    push.on('error', function(e) {
        alert(e);
    });
}

function onDeviceReady(){
    document.addEventListener("backbutton", onBackKeyDown, false);
    if(getLocal("realUbic",true)){
        cordova.plugins.backgroundMode.enable();
        cordova.plugins.backgroundMode.setDefaults({
            title: "Ubic. en tiempo real activa",
            text: "Pulsa aquí para desactivarla",
            hidden: false,
            bigText: false,
            icon: 'icon'
        });
        cordova.plugins.backgroundMode.on('activate', function() {
            cordova.plugins.backgroundMode.disableWebViewOptimizations(); 
        });
        realPos = setInterval(function(){
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = position.coords.latitude + "," + position.coords.longitude;
                var id = getSession(true);
                console.log("SOURCE LOCATED " + pos + " AND " + id.id);
                $.ajax({
                    type: "POST",
                    crossDomain: true,
                    url: getUrl("loc/updatePos"),
                    data: {"pos" : pos , "id" : id.id},
                    success: function (response) {
                        console.log(response);
                        response = JSON.parse(response);
                        if(response.success){
                            // toastSuccess("Notificación enviada",false);
                        }else{
                            // toastError("El usuario no se encuentra disponible",false);
                        }
                    }
                });
            });
        },10000);    
    }else{
        cordova.plugins.backgroundMode.disable();
    }
    getPush(); 
}

function onBackKeyDown() {
    bfn();
}

function setBack(fn){
    bfn = fn;
}


var project = "vanapi";
/* VARIABLES GLOBALES, OBLIGATORIAMENTE DEBE SER EL ULTIMO ARCHIVO EN EL HTML */
/* Retorna el endpoint concatenado con la URL del servicio */

function getSrc(loc) {
    return url + "/" + project + "/" + loc;
}

function getUrl(endpoint) {
    return url + "/" + project + "/?/" + endpoint;
}

/*Crea un mensaje TOAST de toastr */
function toastError(msg, title) {
    if (!title) {
        toastr["error"]("", msg);
    } else {
        toastr["error"](title, msg);
    }
}

function toastSuccess(msg, title) {
    if (!title) {
        toastr["success"]("", msg);
    } else {
        toastr["success"](title, msg);
    }
}

function toastWarning(msg, title) {
    if (!title) {
        toastr["warning"]("", msg);
    } else {
        toastr["warning"](title, msg);
    }
}

function toastInfo(msg, title) {
    if (!title) {
        toastr["info"]("", msg);
    } else {
        toastr["info"](title, msg);
    }
}

/* MANEJO DE SESIONES */

/* Obtiene el objeto "session" de la localStorage, devolverá undefined si una session no esta definida. */
function getSession(parse) {
    return getLocal("session", parse);
}

/* Setea la sesión, y la devuelve como prueba. */
function setSession(ses) {
    setLocal("session", ses);
    return getLocal("session");
}

/* Permite verificar si el localstorage se encuentra disponible y con espacio (limite 5 mb) */
function sAvailable(type) {
    try {
        var storage = window[type],
        x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    } catch (e) {
        return e instanceof DOMException && (
            // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            storage.length !== 0;
        }
    }
    
    /* Setea una variable en el localstorage, y retorna el valor del elemento como prueba. */
    function setLocal(el, data) {
        if (sAvailable('localStorage')) {
            if (typeof data == "object") {
                window.localStorage[el] = JSON.stringify(data);
            } else {
                window.localStorage[el] = data;
            }
            return window.localStorage[el];
        } else {
            toastError("Hay un error con la memoria local de la aplicación...", false);
            return false;
        }
    }
    
    function removeLocal(el) {
        window.localStorage.removeItem(el);
    }
    
    /* Obtiene una variable del localstorage */
    function getLocal(el, parse) {
        if (sAvailable('localStorage')) {
            if (parse) {
                return JSON.parse(window.localStorage[el]);
            } else {
                return window.localStorage[el];
            }
        } else {
            toastError("Hay un error con la memoria local de la aplicación...", false);
            return false;
        }
    }