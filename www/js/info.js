$(document).ready(function(){
    navigator.geolocation.getCurrentPosition(function (position) {
        var smap = $(".map-salvaje");
        var dmap = $("#map");
        appendLoader();
        infomap = loadStaticMap(position.coords.latitude, position.coords.longitude, smap);
        showMap(dmap, smap, position.coords.latitude, position.coords.longitude);
    });
});