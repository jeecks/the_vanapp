$(document).ready(function(){
    setBack(function(){
        $(".back").click();
    });
    setLocal("realUbic",false);
    setLocal("wifiOnly",false);
    $(".validator").on("click", function (e) {
        var a = $(this);
        e.preventDefault();
        e.stopPropagation();
        if (!$("form").valid({
            showErrors: function (errorMap, errorList) {
                return true;
            }
        })) {
            return toastError("Hay errores en el formulario",false);
        }
        $.ajax({
            type: "POST",
            crossDomain: true,
            beforeSend: function () {
                appendLoader();
            },
            url: getUrl("sesion/login"),
            data: $("form").serialize(),
            success: function (response) {
                console.log(response);
                response = JSON.parse(response);
                console.log($(response).get(0));
                if (!response.success) {
                    toastError(response.message,false);
                    removeLoader();
                } else {
                    ses = response.response;
                    setSession(ses);
                    updateKey(function(){
                        removeLoader();
                        if (ses.conductor == 1){
                            var rol = getSession(true);
                            rol.rol = "conductor";
                            setSession(rol);
                            goTo($("<a href='cservices.html'></a>"));
                            return;
                        }else{
                            var rol = getSession(true);
                            rol.rol = "estud";
                            setSession(rol);
                            goTo($("<a href='services.html'></a>"));
                            return;
                        }
                    });
                    
                }
            }
        });
    });
});