$(document).ready(function(){
    var user = JSON.parse(getSession());
    var url = new URL(location.href);
    var from = url.searchParams.get("from");
    var cropit;
    var base64g = false;

    $("#back").attr("href", (from == null ? "services" : from) + ".html");


    $(".photobox").on("click", function () {
        $("#pic").click();
    });

    $("#pic").change(function () {
        if (this.files && this.files[0]) {
            $(".modal-trigger").click();
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
        }
    });

    function imageIsLoaded(e) {
        var img = $('.cropit').attr("src", e.target.result);
        var el = img.get(0);
        cropit = new Croppie(el, {
            enableExif: true,
            viewport: {
                width: 200,
                height: 200,
                type: 'square'
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

    };

    $(".crop-image").on("click", function () {
        console.log(
            $(cropit.result('base64').then(function (base64) {
                $(".photobox").css("background-image", "url(" + base64 + ")").find(".material-icons").hide();
                base64g = base64;
            })));
        cropit.destroy();
    });
    $(".destroy").on("click", function () {
        cropit.destroy();
    });

 
    $('input.valid').prop('disabled', function(i, v) { return !v; });
    $("#save").addClass("hidden");
    $(".switchbutton").click(function (e) { 
        e.preventDefault();
        e.stopPropagation();
        if($(this).attr("id") == "save"){
            if (!$("form").valid({showErrors: function (errorMap, errorList) {return true;}})) {
                return toastError("Hay errores en el formulario",false);
            } else {                
                var data = JSON.stringify({id : user.id, nombre : $("#nombre").val(), apellido : $("#apellido").val(), pic : base64g, ced : $("#cedula").val(), pos : ($(".map-salvaje").attr("data-lat") +","+ $(".map-salvaje").attr("data-lng"))});
                console.log(JSON.parse(data));
                $.ajax({
                    type: "POST",
                    async: false,
                    url: getUrl("profile/save"),                    
                    beforeSend: function () {
                        appendLoader();
                    },           
                    headers: {
                        "content-type": "application/json",
                        "cache-control": "no-cache"
                    },                       
                    data: data,
                    success: function (response) {                        
                        var json = JSON.parse(response); 
                        console.log(json);
                        if(!json.success)
                            toastInfo(json.message, false);        
                        else{
                            var infouser = json.response;
                            var ses = getSession(true);
                            ses.nombre = infouser.nombre;
                            ses.apellido = infouser.apellido;
                            ses.cedula = infouser.cedula;
                            setSession(ses);
                            console.log(json);
                            $("#nombre").val(infouser.nombre);
                            $("#apellido").val(infouser.apellido);
                            $("#cedula").val(infouser.cedula);
                            $('input.valid').prop('disabled', function(i, v) { return !v; });
                            $(".switchbutton").toggleClass("hidden");
                            toastSuccess(json.response, false);
                            loadInfo(user);
                        }
                        removeLoader();
                    }
                });                
            }
        } else {
            $('input.valid').prop('disabled', function(i, v) { return !v; });
            $(".switchbutton").toggleClass("hidden");
            $(".photobox").css("background-image", "");
            $("#change-image").show();
        }        
        
    });

    loadInfo(user);
    
});

function loadInfo(user){

    $.ajax({
        type: "POST",
        async: false,
        url: getUrl("info/getStudentInfo"),
        data: "estud=" + user.id, 
        beforeSend: function () {
            appendLoader();
        },       
        success: function (response) {
            var json = JSON.parse(response);
            console.log(json);
            if(!json.success)
                toastInfo("Error al intentar obtener información", false);        
            else{
                var infouser = json.response;
                var ses = getSession(true);
                ses.nombre = infouser.nombre;
                ses.apellido = infouser.apellido;
                ses.cedula = infouser.cedula;
                setSession(ses);
                $("#nombre").val(infouser.nombre);
                $("#apellido").val(infouser.apellido);
                $("#cedula").val(infouser.cedula);
                $("#change-image").hide();
                $(".photobox").css("background-image", "url(" + getSrc(infouser.imagen) + "?" + Date.now() + ")");
                navigator.geolocation.getCurrentPosition(function (position) {
                    var smap = $(".map-salvaje");
                    var dmap = $("#map");
                    loadStaticMap(parseFloat(infouser.vivienda.split(",")[0]), parseFloat(infouser.vivienda.split(",")[1]), smap);
                    showMap(dmap, smap, parseFloat(infouser.vivienda.split(",")[0]), parseFloat(infouser.vivienda.split(",")[1]));
                });
            }
            removeLoader();
        }
    });
}