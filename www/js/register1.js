$(document).ready(function () {
    $('select').material_select();
    //debo reiniciar el select porque materialize vale verga
    $("select").on('change', function () {
        $('select').material_select();
    });

    setBack(function(){
        $(".back").click();
    });

    navigator.geolocation.getCurrentPosition(function (position) {
        var smap = $(".map-salvaje");
        var dmap = $("#map");
        appendLoader();
        loadStaticMap(position.coords.latitude, position.coords.longitude, smap);
        showMap(dmap, smap, position.coords.latitude, position.coords.longitude);
    });

    $(".validator").on("click", function (e) {
        var a = $(this);
        e.preventDefault();
        e.stopPropagation();
        if (!$("form").valid({
                showErrors: function (errorMap, errorList) {
                    return true;
                }
            })) {
            return toastError("Hay errores en el formulario",false);
        }
        $.ajax({
            type: "POST",
            crossDomain: true,
            beforeSend: function () {
                appendLoader();
            },
            url: getUrl("sesion/validater1"),
            data: $("form").serialize(),
            success: function (response) {
                response = JSON.parse(response);
                removeLoader();
                //console.log(response.success);
                if (!response.success) {
                    toastError("Un usuario ya está usando esa cédula.",false);
                } else {
                    setSession({
                        "first_name": $("#first_name").val(),
                        "last_name": $("#last_name").val(),
                        "tipoced": $("select[name=tipo_ced]").val(),
                        "cedula": $("#cedula").val(),
                        "viv_lat": $(".map-salvaje").attr("data-lat"),
                        "viv_lng": $(".map-salvaje").attr("data-lng")
                    });
                    goTo(a);
                }
            }
        });
    });
});