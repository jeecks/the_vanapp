$(document).ready(function () {
    var cropit;
    var base64g = false;

    setBack(function(){
        $(".back").click();
    });

    $(".photobox").on("click", function () {
        $("#pic").click();
    });
    $('select').material_select();
    //debo reiniciar el select porque materialize vale verga
    $("select").on('change', function () {
        $('select').material_select();
    });
    $("#pic").change(function () {
        if (this.files && this.files[0]) {
            $(".modal-trigger").click();
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
        }
    });

    function imageIsLoaded(e) {
        var img = $('.cropit').attr("src", e.target.result);
        var el = img.get(0);
        cropit = new Croppie(el, {
            enableExif: true,
            viewport: {
                width: 200,
                height: 200,
                type: 'square'
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

    };

    $(".crop-image").on("click", function () {
        console.log(
            $(cropit.result('base64').then(function (base64) {
                $(".photobox").css("background-image", "url(" + base64 + ")").find(".material-icons").hide();
                base64g = base64;
            })));
        cropit.destroy();
    });
    $(".destroy").on("click", function () {
        cropit.destroy();
    });

    $(".validator").on("click", function (e) {
        var a = $(this);
        e.preventDefault();
        e.stopPropagation();
        if (!$("form").valid({
                showErrors: function (errorMap, errorList) {
                    return true;
                }
            })) {
            return toastError("Hay errores en el formulario",false);
        } else if (!base64g) {
            return toastError("Subir una imagen es obligatorio",false);
        }
        $.ajax({
            type: "POST",
            crossDomain: true,
            beforeSend: function () {
                appendLoader();
            },
            url: getUrl("sesion/validater2"),
            data: $("form").serialize(),
            success: function (response) {
                console.log(response);
                response = JSON.parse(response);
                console.log($(response).get(0));
                removeLoader();
                if (!response.success) {
                    toastError("Un usuario está usando ese correo!",false);
                } else {
                    console.log("nani?");
                    var ses = getSession(true);
                    ses["email"] = $("#email").val();
                    ses["pwd"] = $("#password").val();
                    ses["telf"] = $("select[name=cel1]").val() + $("#cel2").val();
                    ses["pic"] = base64g;
                    console.log(ses);
                    setSession(ses);
                    goTo(a);
                }
            }
        });
    });
});