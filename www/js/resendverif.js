$(document).ready(function(){
    $(".validator").on("click", function (e) {
        var a = $(this);
        e.preventDefault();
        e.stopPropagation();
        if (!$("form").valid({
            showErrors: function (errorMap, errorList) {
                return true;
            }
        })) {
            return toastError("Hay errores en el correo proporcionado",false);
        }
        $.ajax({
            async: true,
            crossDomain: true,
            method: "POST",
            beforeSend: function () {
                appendLoader();
            },
            url: getUrl("mailer/activationMail"),
            data: $("form").serialize(),
            success: function (response) {
                response = JSON.parse(response);
                removeLoader();
                if (!response.success) {
                    toastError(response.message,false);
                } else {
                    goTo("emailverif.html",true);
                }
            }
        });
    });
});