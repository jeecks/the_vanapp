appendLoader();
var geoLoca = false;
var geoAct = false;

function addNotifListeners(){
  $(".notification-block").on("click",function(){
    var nombre = getSession(true);
    nombre = nombre.nombre;
    console.log("y entonces?");
    sendNotif(getLocal("idconductor"),nombre + " : " + $(this).html());
    $('#infoTab').click()
  });
  
}

$(document).ready(function () {
  var infomap = false;
  var actualtab = "services";
  var user = JSON.parse(getSession());  
  
  setBack(function(){
    if(confirm("Desea salir de la aplicación?")){
      navigator.app.exitApp();
    }
  });
  
  
  $(".tabs a span.tab-html").html();
  $(".tabs a").on("click", function (e) {
    switch ($(this).attr("data-target")) {
      case "services":
      if(geoLoca){
        clearInterval(geoLoca);
      }
      
      var todos = $("#check0").is(":checked") ? 1 : 0;
      var diurno = $("#check1").is(":checked") ? 1 : 0;
      var vespertino = $("#check2").is(":checked") ? 1 : 0;
      var nocturno = $("#check3").is(":checked") ? 1 : 0;
      getServices(user.instituto_id, todos, diurno, vespertino, nocturno);  
      
      actualtab = "services";
      break;
      case "payments":
      if(geoLoca){
        clearInterval(geoLoca);
      }
      getPayments(user.servicio_id);
      actualtab = "payments";      
      break;
      case "info":
      if(geoLoca){
        clearInterval(geoLoca);
      }
      //aqui va el ajax que se trae los datos de posición del user xd
      if(actualtab != "info"){
        $(".map-salvaje").attr("src","img/loading.gif");
        setTimeout(function(){
          loadStaticMap(10.654450899999999, -71.7147951, $(".map-salvaje"));
        },200);
      }
      getInfo(user.servicio_id);
      actualtab = "info";
      break;
      default:
      break;
    }
  });
  
  $(".switch").find("input[type=checkbox]").prop("checked", true);
  $('ul.tabs').tabs({
    'selected_tab': 'services'
  });
  $(".turno").attr("data-count", "0");
  $('.carousel.carousel-slider').carousel({
    fullWidth: true
  });      
  
  $(".title").on("click", function () {
    $(".filtro").toggleClass("hidden animated fadeIn");
  });
  
  $(".switch").find("input[type=checkbox]").on("change", function () {
    var hermano = $(this).parents(".switch").siblings(".turno");    
    $(hermano).toggleClass("disabled");    
    if($(this).attr("id") == "check0" && $("#check0").is(":checked")){
      $(".switch").find("input[type=checkbox]").prop("checked", true);
      $(".switch").siblings(".turno").removeClass("disabled");
    } else {
      $("#check0").prop("checked", false);  
      if(!($("check0").hasClass("disabled")))
      $("#check0").parents(".switch").siblings(".turno").addClass("disabled");
    }
    
    var todos = $("#check0").is(":checked") ? 1 : 0;
    var diurno = $("#check1").is(":checked") ? 1 : 0;
    var vespertino = $("#check2").is(":checked") ? 1 : 0;
    var nocturno = $("#check3").is(":checked") ? 1 : 0;
    getServices(user.instituto_id, todos, diurno, vespertino, nocturno);    
  });    
  
  getServices(user.instituto_id);  
  
});

function hideFilter(){
  $(".title").hide();
  if($(".filtro").hasClass("fadeIn"))
  $(".filtro").toggleClass("hidden animated fadeIn");
}

function goToNotifications(){
  $("#info").empty();
  $("#info").append($("#notifications").html());
  addNotifListeners();
}

function getServices(idinstituto, check0 = 1, check1 = 1, check2 = 1, check3 = 1){
  $("#servicesList").empty();
  $.ajax({
    type: "POST", 
    data: "instituto=" + idinstituto + "&todos=" + check0 + "&matutino=" + check1 + "&vespertino=" + check2 + "&nocturno=" + check3, 
    async: true,
    crossDomain: true,
    beforeSend: function() {         
      appendLoader(); 
    },
    url: getUrl("services/GetServices"),  
    success: function(response) {      
      var json = JSON.parse(response);   
      var user = JSON.parse(getSession());    
      console.log(json);  
      if(!json.success)
      toastInfo(json.message, false);        
      else{
        $.each(json.response, function (index, service) { 
          var subscribed = false;
          if(user.servicio_id != null){
            subscribed = user.servicio_id == json.idservicio ? true : false;
          }
          if(subscribed)
          {
            $("#btncall").attr("href", service.telefono);
            $("#btnsms").attr("href", service.telefono);
          }
          $("#servicesList").append(
            $('<div/>').addClass("row panelServiceInfo").attr("data-id", service.idservicio).attr("onclick", "getServiceDetail("+service.idservicio+")")
            .append(
              $('<div/>')
              .addClass("col s6 imgParent")
              .append(
                $('<img/>')
                .attr("src", getSrc(service.img))
                .attr("alt", "img")
              )                  
            ).append(
              $('<div/>')
              .addClass("col s6 infoSection")
              .append(
                $('<div/>')
                .addClass("row")
                .append(
                  $('<div/>')
                  .addClass("col s12 row left-align nopadding subtitle")
                  .html('<div class="col s6" style="padding:0">Nombre</div>' + (subscribed == true ? '<div class="col s6"><span style="background-color:#c54d4d; min-width: 0rem;" class=" new badge" data-badge-caption="">Suscrito</span></div>' : ""))
                  .css({"padding-right": "0" , "margin" : "0"})
                )
                .append(
                  $('<div/>')
                  .addClass("col s12 left-align nopadding info")
                  .text(service.nombre + " " + service.apellido)
                )
              )
              .append(
                $('<div/>')
                .addClass("row")
                .append(
                  $('<div/>')
                  .addClass("col s12 left-align nopadding subtitle")
                  .text("Modelo / Placa")
                )
                .append(
                  $('<div/>')
                  .addClass("col s12 left-align nopadding info")
                  .text(service.modelo + " / " + service.placa)
                )
              )
              .append(
                $('<div/>')
                .addClass("row")
                .append(
                  $('<div/>')
                  .addClass("col s12 left-align nopadding subtitle")
                  .text("Disponibilidad")
                )
                .append(
                  $('<div/>')
                  .addClass("col s4 left-align nopadding turno " + (service.matutino == "1" ? "" : "disabled"))
                  .append(
                    $('<i/>')
                    .addClass("material-icons valign-wrapper")
                    .text("brightness_high")
                  )
                )
                .append(
                  $('<div/>')
                  .addClass("col s4 left-align nopadding turno " + (service.vespertino == "1" ? "" : "disabled"))
                  .append(
                    $('<i/>')
                    .addClass("material-icons valign-wrapper")
                    .text("brightness_6")
                  )
                )
                .append(
                  $('<div/>')
                  .addClass("col s4 left-align nopadding turno " + (service.nocturno == "1" ? "" : "disabled"))
                  .append(
                    $('<i/>')
                    .addClass("material-icons valign-wrapper")
                    .text("brightness_4")
                  )
                )
              )
            ));
          }); 
          
          if($("#servicesList").hasClass("scrollableSD"))
          $("#servicesList").removeClass("scrollableSD").addClass("scrollable");
        }
        removeLoader();
      }
    });
    
  }
  
  function getServiceDetail(idservicio){
    hideFilter();
    $("#servicesList").empty();
    $.ajax({
      type: "POST", 
      async: true,
      data: "id=" + idservicio,
      crossDomain: true,
      beforeSend: function() {         
        appendLoader(); 
      },
      url: getUrl("services/GetServiceDetail"),  
      success: function(response) {
        var user = JSON.parse(getSession());  
        var json = JSON.parse(response).response;
        var subscribed = false;
        if(user.servicio_id != null){
          subscribed = user.servicio_id == json.idservicio ? true : false;
        }
        
        console.log(json);
        if (subscribed) {
          $(".abandonBtn").show();
          $(".subscribed-banner").show();
        }
        
        $("#t1").addClass((json.matutino == "0" ? "disabled" : ""));
        $("#t2").addClass((json.vespertino == "0" ? "disabled" : ""));
        $("#t3").addClass((json.nocturno == "0" ? "disabled" : ""));
        $(".vanPic").attr("src",getSrc(json.imagen));
        $("#modelo").text(json.modelo + ", " + json.año);
        $("#placa").text(json.placa);
        $("#capacidad").text(json.asientos_max);
        $("#suscritos").text(json.asientos_usados);
        $("#nombre").text(json.nombre + " " + json.apellido);
        $("#instituto").text(json.nombreinstituto);
        $("#descripcion").text(json.descripcion);
        $("#telefono").html("<i class='v-align-middle material-icons'>chevron_right</i>" + json.telefono).attr("data-phone", json.telefono);
        $("#correo").html("<i class='v-align-middle material-icons'>chevron_right</i>" + json.email).attr("data-email",json.email);
        if(user.servicio_id != null){
          setLocal("idconductor", json.idconductor);          
        }        
        
        $("#servicesList").append($("#serviceDetail").html());      
        
        if($("#servicesList").hasClass("scrollable"))
        $("#servicesList").removeClass("scrollable").addClass("scrollableSD");
        $('.collapsible').collapsible();
        serviceListeners();
        removeLoader();
      }  
    });
  }
  function goToServices() {
    $(".title").show();
    var todos = $("#check0").is(":checked") ? 1 : 0;
    var diurno = $("#check1").is(":checked") ? 1 : 0;
    var vespertino = $("#check2").is(":checked") ? 1 : 0;
    var nocturno = $("#check3").is(":checked") ? 1 : 0;
    getServices(JSON.parse(getSession()).instituto_id, todos, diurno, vespertino, nocturno);
  }
  
  function getPayments(idservicio){
    hideFilter();
    console.log("idservicio: " + idservicio);
    $("#panelFeeInfo").empty();  
    if(idservicio == null){
      $("#panelFeeInfo").append($("#oops").html());
    } else {
      var ses = getSession(true);
      $.ajax({
        type: "POST", 
        async: true,
        data: "stud=" + ses.id,
        crossDomain: true,
        beforeSend: function() {         
          appendLoader(); 
        },
        url: getUrl("payments/GetPaymentDetailStud"),  
        success: function(response) {   
          if(JSON.parse(response).success) {   
            var json = JSON.parse(response).response;
            console.log(json);
            $.each(json, function (index, fee) { 
              if(fee.pagado == 1){
                var recordatorio = "";
              } else {
                var recordatorio = (fee.fecha_reminder == null ? "AGREGAR<br/>RECORDATORIO" : "Recordar el " + getFechaString(fee.fecha_reminder));
              }
              
              $("#panelFeeInfo").append(
                $("<ul/>").addClass("collapsible").attr("data-collapsible", "accordion").css("margin", "0").append(
                  $("<li/>")
                  .append(
                    $("<div/>").addClass("collapsible-header")
                    .append(
                      $("<div/>").addClass("col s10").text(fee.nombre)
                    )
                    .append(
                      $("<div/>").addClass("col s2").append(function(){
                        if(fee.pagado == 1){
                          return $("<span/>").addClass("new badge").attr("data-badge-caption", "").text("Pagado")
                        }else{
                          return $("<span/>").addClass("new badge red").attr("data-badge-caption", "").text("No Pagado")
                        }
                      }
                      
                    )
                  )
                )
                .append(
                  $("<div/>").addClass("collapsible-body").css("padding", "1rem")
                  .append(
                    $("<div/>").addClass("row")
                    .append(
                      $("<div/>").addClass("col s5").css("border-right", "1px solid")
                      .append(
                        $("<div/>").addClass("row").css("height", "100px")
                        .append(
                          $("<div/>").addClass("col s12")
                          .append(
                            (fee.pagado == 1 ? "<i style='color: #26a69a' class='large material-icons'>check_circle</i>" : "<i class='small material-icons'>insert_invitation</i>")                            
                          )
                        )
                        .append(
                          $("<a/>").addClass("datepicker ignore").html(recordatorio).attr("id", fee.cuotas_id).attr("data-idservicio", idservicio)
                        )
                      )
                    )
                    .append(
                      $("<div/>").addClass("col s7")
                      .append(
                        $("<div/>").addClass("row")
                        .append(
                          $("<div/>").addClass("col s12 left-align nopadding subtitle gray small").text("Fecha de Pago")
                        )
                        .append(
                          $("<div/>").addClass("col s12 left-align nopadding info bold").text(getFechaString(fee.fecha))
                        )
                      )
                      .append(
                        $("<div/>").addClass("row")
                        .append(
                          $("<div/>").addClass("col s12 left-align nopadding subtitle gray small").text("Monto")
                        )
                        .append(
                          $("<div/>").addClass("col s12 left-align nopadding info bold").text("BsF " + (parseFloat(fee.cantidad)).toLocaleString('de-DE'))//
                        )
                      )
                    )
                  )
                )
              )
            ); 
          });        
          
          
          $('.collapsible').collapsible();
          $('.datepicker').pickadate({
            min: true,
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 2, // Creates a dropdown of 15 years to control year,
            today: 'Hoy',
            clear: 'Limpiar',
            closeOnSelect: true, // Close upon selecting a date,
            onClose: function () {
              $(document.activeElement).blur();
            }
          });
          
          $(".datepicker").on("change" , function (e) {
            var idservicio = $(this).attr("data-idservicio");
            var date = $(this).val();
            var idcuota = $(this).attr("id");
            $.ajax({
              type: "POST", 
              async: true,
              data: "id=" + idcuota + "&date=" + date,
              crossDomain: true,
              beforeSend: function() {         
                appendLoader(); 
              },
              url: getUrl("payments/SetReminder"),
              success: function (response) {
                var res = JSON.parse(response);                
                if(res.success) {
                  toastSuccess(res.response, false);
                } else {
                  if(res.message == 1)
                  toastInfo("La fecha seleccionada ya ha sido agregada anteriormente.", false);
                  else
                  toastError("Error interno del servidor.", false);
                }                
                $('.picker__close').click();            
                removeLoader();
                getPayments(idservicio);
              }
            });
            
          });
          
        } else{          
          $("#panelFeeInfo").append($("#nofee").html());
        }            
        
        removeLoader();
      }
      
    });
  }
}

function getInfo(idservicio){
  hideFilter();
  $("#info").empty();
  if(idservicio == null){
    $("#info").append($("#oops").html());
  } else {
    $("#info").append($("#default").html());
    $.ajax({
      async: true,
      crossDomain: true,
      method: "POST",
      headers: {
        "content-type": "application/json",
        "cache-control": "no-cache"
      },
      beforeSend: function () {
        appendLoader();
      },
      url: getUrl("info/getPos"),
      data: JSON.stringify({"estud" : getLocal("idconductor")}),
      success: function (response) {
        response = JSON.parse(response); 
        console.log(response);
        if (response.success) {
          var stud = response.response;
          if (stud.actualpos != null) {
            $(".realActive").show();
            $(".realInactive").hide();
            geoLoc(response);
          } else {
            $(".realActive").hide();
            $(".realInactive").show();
          }
        } else {
          toastError("Error de servicio...");
        }
        removeLoader();
      }
    });
    reApplyListeners();
  }
  
  
  function geoLoc(responsel) {
    $.ajax({
      async: true,
      crossDomain: true,
      method: "POST",
      headers: {
        "content-type": "application/json",
        "cache-control": "no-cache"
      },
      beforeSend: function () {
        appendLoader();
      },
      url: getUrl("info/getPos"),
      data: JSON.stringify({"estud" : getLocal("idconductor")}),
      success: function (response) {
        response = JSON.parse(response);
        console.log(response);
        $(".studentActions").show();
        if (response.success) {
          if (!geoAct) {
            var pos = response.response.actualpos;
            var lat = pos.substr(0, pos.indexOf(','));
            var lng = pos.substr(pos.indexOf(',') + 1);
            loadStaticMap(parseFloat(lat), parseFloat(lng), $(".map-salvaje"));
            showMapNoSelect($("#map"), $(".map-salvaje"), parseFloat(lat), parseFloat(lng));
            geoAct = true;
          } else {
            var pos = response.response.actualpos;
            var lat = pos.substr(0, pos.indexOf(','));
            var lng = pos.substr(pos.indexOf(',') + 1);
            var myLatlng = new google.maps.LatLng(lat, lng);
            placeMarkerStat(myLatlng, $(".map-salvaje"));
          }
        } else {
          toastError("Error de servicio...");
        }
        removeLoader();
      }
    });
    geoLoca = setInterval(function () {
      $.ajax({
        async: true,
        crossDomain: true,
        method: "POST",
        headers: {
          "content-type": "application/json",
          "cache-control": "no-cache"
        },
        beforeSend: function () {
          appendLoader();
        },
        url: getUrl("info/getPos"),
        data: JSON.stringify({"estud" : getLocal("idconductor")}),
        success: function (response) {
          response = JSON.parse(response);
          console.log(response);
          $(".studentActions").show();
          if (response.success) {
            if (!geoAct) {
              var pos = response.response.actualpos;
              var lat = pos.substr(0, pos.indexOf(','));
              var lng = pos.substr(pos.indexOf(',') + 1);
              loadStaticMap(parseFloat(lat), parseFloat(lng), $(".map-salvaje"));
              showMapNoSelect($("#map"), $(".map-salvaje"), parseFloat(lat), parseFloat(lng));
              geoAct = true;
            } else {
              var pos = response.response.actualpos;
              var lat = pos.substr(0, pos.indexOf(','));
              var lng = pos.substr(pos.indexOf(',') + 1);
              var myLatlng = new google.maps.LatLng(lat, lng);
              placeMarkerStat(myLatlng, $(".map-salvaje"));
            }
          } else {
            toastError("Error de servicio...");
          }
          removeLoader();
        }
      });
    }, 10000);
    
  }  
}

function abandonService() {
  var user = getSession(true);
  $.ajax({
    async: false,       
    method: "POST",        
    beforeSend: function () {
      appendLoader();
    },
    url: getUrl("services/AbandonService"),
    data: "iduser=" + user.id,
    success: function (response) {
      var json = JSON.parse(response);                        
      if (json.success) {
        toastSuccess("¡Has abandonado el servicio!", false);
        setTimeout(2000,refreshSes());
      } else {
        toastError(json.message, false);
      }
      removeLoader();
    }
  }); 
}

function getFechaString(date){
  var d = new Date(date);
  var fechaString = "";
  fechaString = getDayOfTheWeek(d) + ", " + date.split("-")[2] + " de " + getMonthOfYear(date.split("-")[1]) + " de " + date.split("-")[0];
  return fechaString;
}

function getDayOfTheWeek(d){    
  var weekday = new Array(7);  
  weekday[0] = "Lunes";
  weekday[1] = "Martes";
  weekday[2] = "Miercoles";
  weekday[3] = "Jueves";
  weekday[4] = "Viernes";
  weekday[5] = "Sábado";
  weekday[6] = "Domingo";
  return weekday[d.getDay()];
}

function getMonthOfYear(monthNumber){
  var month = new Array(12);
  month[0] = "Enero";
  month[1] = "Febrero";
  month[2] = "Marzo";
  month[3] = "Abril";
  month[4] = "Mayo";
  month[5] = "Junio";
  month[6] = "Julio";
  month[7] = "Agosto";
  month[8] = "Septiembre";
  month[9] = "Octubre";
  month[10] = "Noviembre";
  month[11] = "Diciembre";
  
  return month[parseInt(monthNumber)];
}