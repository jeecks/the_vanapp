var filename = location.pathname.substr(location.pathname.lastIndexOf("/")+1).split(".")[0];
var nave = '<ul id="slide-out" class="side-nav">'+
'<li><div class="user-view row z-depth-2">'+
' <a class="col s4 user-meta"><img class="user-pic" src="http://via.placeholder.com/150x150"><br><span class="user-code">UNDEFINED</span></a>'+
'<a class="col s8"><p class="white-text user-name"><br></p></a>'+
' </div></li>'+
' <li><a href="profile.html?from='+filename+'"><i class="material-icons">&#xE7FD;</i>Mi Perfil</a></li>'+
'<li><a href="changepass.html?from='+filename+'"><i class="material-icons">&#xE897;</i>Cambiar Contraseña</a></li>'+
'<li><a href="changeinstitute.html?from='+filename+'"><i class="material-icons">&#xE569;</i>Cambiar Instituto</a></li>'+
'<li><a href="config.html?from='+filename+'"><i class="material-icons">&#xE8B8;</i>Configuración</a></li>'+
'<li><a class="cerrarSesion" href="#!"><i class="material-icons">&#xE879;</i>Cerrar Sesión</a></li>'+
'</ul>';

var navc = '<ul id="slide-out" class="side-nav">'+
'<li><div class="user-view row z-depth-2">'+
' <a class="col s4 user-meta"><img class="user-pic" src="http://via.placeholder.com/150x150"><br><span class="user-code">UNDEFINED</span></a>'+
'<a class="col s8"><p class="white-text user-name"><br></p></a>'+
' </div></li>'+
' <li><a href="profile.html?from='+filename+'"><i class="material-icons">&#xE7FD;</i>Mi Perfil</a></li>'+
'<li><a href="astudent.html"><i class="material-icons left">&#xE84E;</i>Asociar Estudiante</a></li>'+
'<li><a href="changepass.html?from='+filename+'"><i class="material-icons">&#xE897;</i>Cambiar Contraseña</a></li>'+
'<li><a href="changeinstitute.html?from='+filename+'"><i class="material-icons">&#xE569;</i>Cambiar Instituto</a></li>'+
'<li><a href="config.html?from='+filename+'"><i class="material-icons">&#xE8B8;</i>Configuración</a></li>'+
'<li><a class="cerrarSesion" href="#!"><i class="material-icons">&#xE879;</i>Cerrar Sesión</a></li>'+
'</ul>';

$(document).ready(function(){
    //agregamos el nav
    var ses = getSession(true);
    if($("body#conductor").length > 0){
        nav = navc;
    }else{
        nav = nave;
    }
    $("body").append($(nav));
    $(".user-name").html(ses.nombre + "<br>" + ses.apellido);
    $(".user-pic").attr("src", getSrc(ses.imagen) + "?" + Date.now());
    $(".user-code").html(ses.id);
    
    $(".cerrarSesion").on("click",function(){
        var id;
        if(getSession(true)){
            id = getSession(true);
            id = id.id;
        }else{
            return false;
        }
        $.ajax({
            type: "POST",
            crossDomain: true,
            beforeSend: function () {
                appendLoader();
            },
            url: getUrl("notif/removeDevice"),
            data: {"id" : id},
            success: function (response) {
                removeLocal("session");
                window.location.href = "index.html";
            }
        });
    });
    // Initialize collapse button
    $(".button-collapse").sideNav();
});