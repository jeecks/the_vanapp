//flags
var alreadyAuto = false;
if (window.cordova != undefined) {
  document.addEventListener("deviceready", onDeviceReady, false);
} else {
  $(document).ready(function () {
    onDeviceReady();
  })
}

setBack(function(){
  if(confirm("Desea salir de la aplicación?")){
    navigator.app.exitApp();
  }
});

function fadeOut(ele,ms,callback){
  if(callback){
    $(ele).transition({
      "opacity" : 0
    },ms,function(){
      callback();
    })
  }else{
    $(ele).transition({
      "opacity" : 0
    },ms);
  }
}

function onDeviceReady() {
  $("a").on("click",function(e){
    var el = $(this);
    e.preventDefault();
    e.stopPropagation();
    fadeOut(".container",300,function(){
      window.location.href = $(el).attr("href");
    })
  });
  var logo = $(".logo");
  
  
  logo.transition({
    "opacity": "1.0",
  }, 1000, function () {
    logo.transition({
      "margin-top": "50%"
    }, 1000, function () {
      logo.transition({
        "margin-top": "1%"
      }, 1000, function () {
        var ses = getSession(false);
        if(typeof ses != "undefined"){
          ses = JSON.parse(ses);
          if(typeof ses.activo != "undefined"){
            if(ses.activo == 1){
              updateKey(function(){
                appendLoader();
                refreshSes();
                return;
              });
            }else{
              $(".btn").transition({
                opacity:1
              },500);
            }
          }else{
            $(".btn").transition({
              opacity:1
            },500);
          }
        }else{
          $(".btn").transition({
            opacity:1
          },500);
        }
      });
      setTimeout(function () {
      }, 500);
    });
  });
  
  $(".slide").click(function (e) {
    e.preventDefault();
    if (window.cordova != undefined) {
      window.plugins.nativepagetransitions.slide({
        // the defaults for direction, duration, etc are all fine
        "href": "boi.html"
      });
    } else {
      window.location.href = "boi.html";
    }
  });
  
  /*cordova.plugins.notification.local.schedule({
    title: "New Message",
    message: "Hi, are you ready? We are waiting."
  });*/
}




